//Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//це технологія взаємодії із сервером. В основному технологія використовується для підвантаження окремих даних, відправки даних форм, а саме авторизація, добавлення коментарів чи відправки повідомлень.

//Завдання:

let urlRequest ='https://ajax.test-danit.com/api/swapi/films/';

function renderNames(arr, container) {
    let output = '<h1>Список серій "Зоряні війни":</h1>';
    arr.forEach(item => {
        output += `
            <ul class="film_card" data-id="${item.episodeId}">
                <li><b>Назва фільму:</b> "${item.name}"</li>
                <li><b>Епізод:</b> "${item.episodeId}"</li>
                <li><b>Зміст:</b> "${item.openingCrawl}"</li>
            </ul>
        `;
    });
    output += '';
    document.querySelector(container).innerHTML = output;

    const lists = document.querySelectorAll(".film_card")
    lists.forEach( list => {list.style.listStyle = "none"})
}

function renderChar(arr) {
  arr.forEach(item => { 
      const cards = document.querySelectorAll(`.film_card`);
      cards.forEach (card => {

        if (card.getAttribute('data-id')===`${item.episodeId}`) { 
          Promise.all(item.characters.map(loadCh)).then(values => {
            const title=card.querySelector("li:first-child")
            title.insertAdjacentHTML('afterend', `
              <li><b>Акторський склад:</b> "${values.map(value => value.name).join(', ')}"</li>
            `);
          }).catch(error => console.log(error));
        } 
      }); 
  });
}

function fetchData(URL) {
  return fetch(URL)
    .then(response => {
      if (!response.ok) {
        throw new Error ('No response');
      }
      return response.json();
    })
    .catch(error => console.error(error));
}

function loadChar(URL) {
  return fetchData(URL)
    .then(data => renderChar(data));
}

function loadCh(URL) {
  return fetchData(URL);
}
 

fetchData(urlRequest)
  .then(data => Promise.all([
    renderNames(data, '.film_episodes'),
    loadChar(urlRequest)
  ]))
  .catch(error => console.error(error));
